/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/09 16:12:00 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/09 16:28:58 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

using std::cout; using std::endl;
using std::toupper;

static void
print_uppercase(const char *str)
{
	for (int i = 0; str[i]; i++)
		cout << (unsigned char)toupper(str[i]);
}

int
main(int argc, char *argv[])
{
	if (argc == 1)
	{
		cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << endl;
		return (0);
	}

	for (int i = 1; i < argc; i++)
		print_uppercase(argv[i]);
	cout << endl;
	return (0);
}
