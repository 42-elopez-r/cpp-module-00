/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/09 17:18:47 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/13 19:25:26 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTACT_HPP
#define CONTACT_HPP

#include <string>
#include "Birthday.hpp"

using std::string;

class Contact
{
	private:
		string first_name;
		string last_name;
		string nickname;
		string login;
		string postal_address;
		string email;
		string phone_number;
		Birthday birthday;
		string favorite_meal;
		string underwear_color;
		string darkest_secret;

		bool is_valid_phone_number();
		bool is_valid_email();
	public:
		Contact();

		void read_contact();
		void display_contact();

		// Getters
		string get_first_name();
		string get_last_name();
		string get_nickname();
		string get_login();
		string get_postal_address();
		string get_email();
		string get_phone_number();
		Birthday get_birthday();
		string get_favorite_meal();
		string get_underwear_color();
		string get_darkest_secret();
};

#endif
