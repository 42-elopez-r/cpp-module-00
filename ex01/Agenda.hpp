/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Agenda.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/12 00:48:32 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/19 19:26:05 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AGENDA_HPP
#define AGENDA_HPP

#include "Contact.hpp"

#define MAX_CONTACTS 8

class Agenda
{
	private:
		Contact contacts[MAX_CONTACTS];
		int in_use;

		void add();
		void display_search_table();
		void search();
		void rotate_and_delete_first();
	public:
		Agenda();
		void mainloop();
};

#endif
