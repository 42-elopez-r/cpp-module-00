/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/12 16:37:30 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/13 17:34:40 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>

using std::string;

void
rtrim(string &str, const char *chars = " \n");

string
cut_with_dot(string str, size_t max_len);

bool
is_natural_number(string str);

#endif
