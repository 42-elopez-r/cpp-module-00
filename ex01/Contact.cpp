/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/09 17:40:42 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/14 20:30:42 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.hpp"
#include "utils.hpp"
#include <iostream>
#include <cstring>
#include <cctype>
#include <regex>

using std::cout; using std::endl;
using std::cin; using std::regex;
using std::regex_match;

/*
 * This method checks if Contact::phone_number has a valid lenght and
 * is only composed by digits.
 */
bool
Contact::is_valid_phone_number()
{
	if (phone_number.length() != 9)
		return (false);
	for (size_t i = 0; i < phone_number.length(); i++)
		if (!isdigit(phone_number[i]))
			return (false);
	return (true);
}

/*
 * This method checks if Contact::email is a valid email address with:
 * - Non empty user
 * - @
 * - Non empty domain label with letters, numbers and hyphens
 * - non empty TLD with several or one dot
 */
bool
Contact::is_valid_email()
{
	return (regex_match(email,
				regex("^.+@([a-z0-9]-*)+(\\.[a-z0-9]+)+$")));
}

Contact::Contact()
{
	first_name.clear();
	last_name.clear();
	nickname.clear();
	login.clear();
	postal_address.clear();
	email.clear();
	phone_number.clear();
	birthday = Birthday();
	favorite_meal.clear();
	underwear_color.clear();
	darkest_secret.clear();
}

/*
 * This method prompts the user to fill all the fields of the class.
 */
void
Contact::read_contact()
{
	cout << "First name: ";
	getline(cin, first_name);
	rtrim(first_name);
	cout << "Last name: ";
	getline(cin, last_name);
	rtrim(last_name);
	cout << "Nickname: ";
	getline(cin, nickname);
	rtrim(nickname);
	cout << "Login: ";
	getline(cin, login);
	rtrim(login);
	cout << "Postal address: ";
	getline(cin, postal_address);
	rtrim(postal_address);
	do
	{
		cout << "Email address: ";
		getline(cin, email);
		rtrim(email);
	}
	while (!cin.eof() && !is_valid_email());
	do
	{
		cout << "Phone number: ";
		getline(cin, phone_number);
	}
	while (!cin.eof() && !is_valid_phone_number());
	while (!cin.eof() && !birthday.read_birthday())
		cout << "Invalid birthday" << endl;
	cout << "Favorite meal: ";
	getline(cin, favorite_meal);
	rtrim(favorite_meal);
	cout << "Underwear color: ";
	getline(cin, underwear_color);
	rtrim(underwear_color);
	cout << "Darkest secret: ";
	getline(cin, darkest_secret);
	rtrim(darkest_secret);
}

/*
 * This method displays all the fields of the class.
 */
void
Contact::display_contact()
{
	cout << "First name: " << first_name << endl;
	cout << "Last name: " << last_name << endl;
	cout << "Nickname: " << nickname << endl;
	cout << "Login: " << login << endl;
	cout << "Postal address: " << postal_address << endl;
	cout << "Email address: " << email << endl;
	cout << "Phone number: " << phone_number << endl;
	birthday.display_birthday();
	cout << "Favorite meal: " << favorite_meal << endl;
	cout << "Underwear color: " << underwear_color << endl;
	cout << "Darkest secret: " << darkest_secret << endl;
}

string
Contact::get_first_name()
{
	return (first_name);
}

string
Contact::get_last_name()
{
	return (last_name);
}

string
Contact::get_nickname()
{
	return (nickname);
}

string
Contact::get_login()
{
	return (login);
}

string
Contact::get_postal_address()
{
	return (postal_address);
}

string
Contact::get_email()
{
	return (email);
}

string
Contact::get_phone_number()
{
	return (phone_number);
}

Birthday
Contact::get_birthday()
{
	return (birthday);
}

string
Contact::get_favorite_meal()
{
	return (favorite_meal);
}

string
Contact::get_underwear_color()
{
	return (underwear_color);
}

string
Contact::get_darkest_secret()
{
	return (darkest_secret);
}
