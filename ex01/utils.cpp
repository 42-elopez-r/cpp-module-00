/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/12 16:36:45 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/13 17:55:01 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.hpp"
#include <cstring>
#include <cctype>

/*
 * This function removes the characters at the end of str that are contained in
 * the chars string.
 */
void
rtrim(string &str, const char *chars)
{
	long long i;

	if (str.length() == 0)
		return;
	i = str.length() - 1;
	while (i >= 0 && strchr(chars, str[i]))
		str.erase(i--);
}

/*
 * This function cuts the passed string if its longer than max_len and
 * replaces the last character with a dot.
 */
string
cut_with_dot(string str, size_t max_len)
{
	if (str.length() <= max_len)
		return (str);
	else
		return (str.replace(max_len - 1, string::npos, "."));
}

/*
 * This function returns true if the passed string is only composed by
 * digits.
 */
bool
is_natural_number(string str)
{
	for (size_t i = 0; i < str.length(); i++)
	{
		if (!isdigit(str[i]))
			return (false);
	}
	return (str.length() > 0);
}
