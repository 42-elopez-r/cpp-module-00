/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Birthday.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/09 17:46:16 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/10 19:54:14 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BIRTHDAY_HPP
#define BIRTHDAY_HPP

class Birthday
{
	private:
		unsigned int day;
		unsigned int month;
		unsigned int year;
	public:
		Birthday();

		bool read_birthday();
		void display_birthday();

		// Getters
		unsigned int get_day();
		unsigned int get_month();
		unsigned int get_year();
};

#endif
