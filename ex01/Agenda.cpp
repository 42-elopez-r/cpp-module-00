/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Agenda.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/12 16:30:28 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/19 19:26:13 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Agenda.hpp"
#include "utils.hpp"
#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>

using std::cin; using std::getline;
using std::string; using std::cout;
using std::endl; using std::setw;
using std::string;

Agenda::Agenda()
{
	in_use = 0;
}

/*
 * This method adds a new contact to the array, after checking if its full.
 */
void
Agenda::add()
{
	string input;

	if (in_use == MAX_CONTACTS)
	{
		cout << "The limit of " << MAX_CONTACTS << " contacts has been reached";
		cout << endl << "Do you wish to overwrite the firt contact to ";
		cout << "make room for a new one?" << endl;
		do
		{
			cout << "[y/n] > ";
			getline(cin, input);
			rtrim(input);
		}
		while (input.compare("y") != 0 && input.compare("n") != 0);
		if (input.compare("y") == 0)
		{
			rotate_and_delete_first();
			in_use--;
		}
		else
			return;
	}
	contacts[in_use].read_contact();
	if (in_use < MAX_CONTACTS)
		in_use++;
}

/*
 * This method displays the table for the SEARCH functionality with a
 * resume of the stored contacts.
 */
void
Agenda::display_search_table()
{
	cout << "---------------------------------------------" << endl;
	cout << "|" << setw(10) << "index" << "|" << setw(10) << "first name";
	cout << "|" << setw(10) << "last name" << "|" << setw(10) << "nickname";
	cout << "|" << endl;
	cout << "---------------------------------------------" << endl;

	for (int i = 0; i < in_use; i++)
	{
		cout << "|" << setw(10) << i << "|";
		cout << setw(10) << cut_with_dot(contacts[i].get_first_name(), 10);
		cout << "|" << setw(10) << cut_with_dot(contacts[i].get_last_name(), 10);
		cout << "|" << setw(10) << cut_with_dot(contacts[i].get_nickname(), 10);
		cout << "|" << endl;
		cout << "---------------------------------------------" << endl;
	}
}

/*
 * This method shows the user a table with the stored contacts and then it
 * prompts to select one to display its full data.
 */
void
Agenda::search()
{
	string input;
	bool valid;

	if (in_use == 0)
	{
		cout << "No contacts stored" << endl;
		return;
	}
	
	display_search_table();

	valid = false;
	while (!valid)
	{
		cout << "Index > ";
		getline(cin, input);
		rtrim(input);

		if (is_natural_number(input) && atoi(input.c_str()) < in_use)
			valid = true;
		else
			cout << input << ": invalid index" << endl;
	}
	contacts[atoi(input.c_str())].display_contact();
}

/*
 * This method moves all the contacts one position down overwriting
 * the first element.
 */
void
Agenda::rotate_and_delete_first()
{
	for (int i = 1; i < MAX_CONTACTS; i++)
		contacts[i - 1] = contacts[i];
}

/*
 * This method keeps asking the user for commands until an EXIT is introduced.
 */
void
Agenda::mainloop()
{
	string input;
	bool keep;

	cout << "Welcome to the wonders of the Casio World Contacts 2000 (tm)!";
	cout << endl << "Available commands: ADD, SEARCH, EXIT" << endl;

	keep = true;
	while (keep && !cin.eof())
	{
		cout << "> ";
		getline(cin, input);
		rtrim(input);

		if (input.compare("EXIT") == 0)
			keep = false;
		else if (input.compare("ADD") == 0)
			add();
		else if (input.compare("SEARCH") == 0)
			search();
		else
		{
			cout << "Invalid command: " << input << endl;
			cout << "Available commands: ADD, SEARCH, EXIT" << endl;
		}
	}
}
