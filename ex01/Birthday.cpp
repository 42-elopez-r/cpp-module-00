/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Birthday.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/09 18:21:13 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/10 19:56:26 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <cstdio>
#include "Birthday.hpp"

using std::cout; using std::string;
using std::cin; using std::endl;

Birthday::Birthday()
{
	day = 1;
	month = 1;
	year = 0;
}

/*
 * This method reads the birthday from stdin and returns true if it was valid.
 * If invalid, returns false and doesn't alter the object.
 */
bool
Birthday::read_birthday()
{
	unsigned int day, month, year;
	string input;

	cout << "Birthday (dd/mm/yyyy): ";
	getline(cin, input);
	
	if (sscanf(input.c_str(), "%2u/%2u/%4u\n", &day, &month, &year) != 3)
		return (false);
	if (day == 0 || day > 31 || month == 0 || month > 12)
		return (false);

	this->day = day;
	this->month = month;
	this->year = year;
	return (true);
}

void
Birthday::display_birthday()
{
	cout << "Birthday: " << day << "/" << month << "/" << year << endl;
}

unsigned int
Birthday::get_day()
{
	return (day);
}

unsigned int
Birthday::get_month()
{
	return (month);
}

unsigned int
Birthday::get_year()
{
	return (year);
}
